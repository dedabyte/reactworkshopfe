import { IYoutubeItem, IActionObject, IStateFiles } from '../types';
import { storage, StorageKeys } from '../storage/storage';
import { FilesActions } from '../actions/files';
import { reductor } from './utils';

// main 'switch' and handler functions

const rGetSavedFiles = (state: IStateFiles, action: IActionObject<FilesActions, IYoutubeItem[]>): IStateFiles => {
  return {
    ...state,
    savedFiles: action.payload as IYoutubeItem[]
  };
}

const rAddFileToMyList = (state: IStateFiles, action: IActionObject<FilesActions, IYoutubeItem>): IStateFiles => {
  const newFile = updateFileWhenAddedOrRemoved(state.searchedFiles, action.payload as IYoutubeItem);
  const savedFiles = ([] as IYoutubeItem[]).concat(state.savedFiles, newFile);

  // not a good practice, but stays here for now...
  storage.set(StorageKeys.myList, savedFiles);

  return {
    ...state,
    savedFiles
  };
}

const rRemoveFileFromMyList = (state: IStateFiles, action: IActionObject<FilesActions, IYoutubeItem>): IStateFiles => {
  updateFileWhenAddedOrRemoved(state.searchedFiles, action.payload as IYoutubeItem);
  const savedFiles = state.savedFiles.filter((file) => {
    return file.etag !== (action.payload as IYoutubeItem).etag;
  });

  // not a good practice, but stays here for now...
  storage.set(StorageKeys.myList, savedFiles);

  return {
    ...state,
    savedFiles
  };
}

const rClearMyList = (state: IStateFiles, action: IActionObject<FilesActions, undefined>): IStateFiles => {
  state.savedFiles.forEach((file) => {
    file.saved = false;
    updateFileWhenAddedOrRemoved(state.searchedFiles, file);
  });
  const savedFiles: IYoutubeItem[] = [];

  // not a good practice, but stays here for now...
  storage.set(StorageKeys.myList, savedFiles);

  return {
    ...state,
    savedFiles
  };
}

const rRequestSearch = (state: IStateFiles, action: IActionObject<FilesActions, string>): IStateFiles => {
  return {
    ...state,
    searchTerm: action.payload as string,
    searchInProgress: true
  };
}

const rResponseSearch = (state: IStateFiles, action: IActionObject<FilesActions, IYoutubeItem[]>): IStateFiles => {
  const searchedFiles = updateFilesOnSearch(action.payload as IYoutubeItem[], state.savedFiles);

  return {
    ...state,
    searchedFiles,
    searchInProgress: false
  }
}

const rClearSearch = (state: IStateFiles): IStateFiles => {
  return {
    ...state,
    searchedFiles: [],
    searchInProgress: false,
    searchTerm: ''
  }
}

const switcher: {
  [actionName in FilesActions]: (state: IStateFiles, action: IActionObject<FilesActions, any>) => IStateFiles
} = {
  [FilesActions.GET_SAVED_FILES]: rGetSavedFiles,
  [FilesActions.ADD_FILE_TO_MY_LIST]: rAddFileToMyList,
  [FilesActions.REMOVE_FILE_FROM_MY_LIST]: rRemoveFileFromMyList,
  [FilesActions.CLEAR_MY_LIST]: rClearMyList,
  [FilesActions.REQUEST_SEARCH]: rRequestSearch,
  [FilesActions.RESPONSE_SEARCH]: rResponseSearch,
  [FilesActions.CLEAR_SEARCH]: rClearSearch,
}

// REDUCER function

/**
 * Initial state.
 */
const initialState: IStateFiles = {
  searchTerm: '',
  searchInProgress: false,
  searchedFiles: [],
  savedFiles: [],
};

/**
 * Just a main reducer function.
 * @param state previous state
 * @param action acton object
 */
export default function (state: IStateFiles = initialState, action: IActionObject<FilesActions, any>): IStateFiles {
  return reductor(switcher, state, action);
}

// UTILS

/**
 * Updates reference to searched file when added/removed to my list to indicate state change and re-render.
 * Returns new file.
 * @param searchedFiles files returned as search results
 * @param payloadFile file from action object
 */
const updateFileWhenAddedOrRemoved = (searchedFiles: IYoutubeItem[], payloadFile: IYoutubeItem): IYoutubeItem => {
  for (let i = 0; i < searchedFiles.length; i++) {
    const searchedFile = searchedFiles[i];
    if (searchedFile === payloadFile) {
      const newFile = {
        ...payloadFile
      };
      searchedFiles[i] = newFile;
      return newFile;
    }
  }
  return payloadFile;
}

/**
 * Updates files returned as search results - marks them as `saved` if found in saved files list;
 * Returns existing list of `searchedFiles`, mutates items.
 * @param searchedFiles files returned as search results
 * @param savedFiles files saved in my list
 */
const updateFilesOnSearch = (searchedFiles: IYoutubeItem[], savedFiles: IYoutubeItem[]): IYoutubeItem[] => {
  savedFiles.forEach((savedFile) => {
    for (const searchedFile of searchedFiles) {
      if (searchedFile.etag === savedFile.etag) {
        searchedFile.saved = true;
        break;
      }
    }
  });
  return searchedFiles;
}
