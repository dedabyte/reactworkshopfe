import {combineReducers, Reducer} from 'redux';
import files from './files';
import modal from './modal';
import { IState } from '../types';

const rootReducer: Reducer<IState> = combineReducers({
  files,
  modal
});

export default rootReducer;
