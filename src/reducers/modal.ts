import { IStateModal, IActionObject } from "../types";
import { ModalActions } from "../actions/modal";
import { reductor } from "./utils";

const rOpenModal = (state: IStateModal, action: IActionObject<ModalActions, IStateModal>): IStateModal => {
  return {
    ...action.payload as IStateModal,
    isActive: true
  }
}

const rCloseModal = (): IStateModal => {
  return {
    ...initialState,
    isActive: false
  }
}

const switcher: {
  [actionName in ModalActions]: (state: IStateModal, action: IActionObject<ModalActions, any>) => IStateModal
} = {
  [ModalActions.OPEN_MODAL]: rOpenModal,
  [ModalActions.CLOSE_MODAL]: rCloseModal,
}

/**
 * Initial state.
 */
const initialState: IStateModal = {
  isActive: false,
  title: '',
  body: '',
  buttons: []
};

/**
 * Just a main reducer function.
 * @param state previous state
 * @param action acton object
 */
export default function (state: IStateModal = initialState, action: IActionObject<ModalActions, any>): IStateModal {
  return reductor(switcher, state, action);
}
