import { IActionObject } from '../types';

interface ISwitcher {
  [actionName: string]: (state: any, action: IActionObject<any, any>) => any
}

export const reductor = (switcher: ISwitcher, state: any, action: IActionObject<any, any>) => {
  if (switcher.hasOwnProperty(action.type)) {
    return switcher[action.type].call(null, state, action);
  }
  return rDefault(state);
}

const rDefault = (state: any) => state;
