import * as React from 'react';
import './App.css';
import Header from './components/header/Header';
import Search from './components/search/Search';
import SavedFiles from './components/savedFiles/SavedFiles';
import {Switch, Route, Redirect, withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import { aGetSavedFiles } from './actions/files';
import Modal from './components/modal/Modal';
import { IState } from './types';

interface IPropsProperties {
  isModalActive: boolean;
}

interface IPropsActions {
  aGetSavedFiles: typeof aGetSavedFiles
}

interface IPropsApp extends IPropsProperties, IPropsActions {}

interface IRouteConfig {
  path: string;
  component: JSX.Element;
  redirectTo: string;
}


export const paths = {
  root: '/search/:searchTerm?',
  login: '/login',
  savedFiles: '/savedfiles'
}

const routes: IRouteConfig[] = [
  {
    path: paths.root,
    component: <Search/>,
    redirectTo: paths.login
  },
  {
    path: paths.savedFiles,
    component: <SavedFiles/>,
    redirectTo: paths.login
  }
];

// todo kad dodje auth
const isAuth = true;

const routeRenderFunc = (route: IRouteConfig) => () => {
  return isAuth ? route.component : <Redirect to={route.redirectTo}/>
};

class App extends React.PureComponent<IPropsApp> {

  public componentDidMount() {
    this.props.aGetSavedFiles();
  }

  public render() {
    return (
      <div className="App">
        <Header/>
        <Switch>
          {
            routes.map(
              (route) => <Route exact={true}
                                key={route.path}
                                path={route.path}
                                render={routeRenderFunc(route)}/>
            )
          }
          <Redirect to={paths.root} from="/"/>
          // todo Login component
        </Switch>
        { this.props.isModalActive? <Modal/> : null }
      </div>
    );
  }
}

const mapStateToProps = (state: IState): IPropsProperties => {
  return {
    isModalActive: state.modal.isActive as boolean
  }
}

const actionsToProps: IPropsActions = {
  aGetSavedFiles,
}

export default withRouter(connect(mapStateToProps, actionsToProps)(App) as any);
