export enum StorageKeys {
  myList = 'myList'
}

class LocalStorageService {
  // prefix for keys in local storage
  private keyPrefix: string;
  // linker between prefix and provided key, eg: cs.myNewKey
  private keyLink: string;

  constructor() {
    this.keyPrefix = 'react-test';
    this.keyLink = '.';
  }

  /**
   * Set value to local storage
   * @param {string} key
   * @param {any} value
   */
  public set(key: StorageKeys, value: any): void {
    localStorage.setItem(this.prefixKey(key), JSON.stringify(value));
  }

  /**
   * Get value from local storage
   * @param {string} key
   * @returns {any}
   */
  public get(key: StorageKeys): any {
    const value = localStorage.getItem(this.prefixKey(key));
    if (value) {
      return JSON.parse(value);
    }
    return undefined;
  }

  /**
   * Remove value from local storage
   * @param {string} key
   */
  public remove(key: StorageKeys): void {
    localStorage.removeItem(this.prefixKey(key));
  }

  /**
   * Prefix the given key with keyPrefix string + keyLink symbol
   * @param {string} key
   * @returns {string}
   */
  private prefixKey(key: StorageKeys): string {
    return this.keyPrefix + this.keyLink + key;
  }
}

export const storage = new LocalStorageService();
