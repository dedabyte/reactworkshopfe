declare namespace Reactiviranje {

  interface IYoutubeItem {
    saved?: boolean;

    etag: string;
    id: {
      kind: string;
      videoId: string;
    };
    snippet: {
      channelId: string;
      channelTitle: string;
      description: string;
      publishedAt: string;
      title: string;
      thumbnails: {
        default: IYoutubeItemThumbnail;
        medium: IYoutubeItemThumbnail;
        high: IYoutubeItemThumbnail;
      }
    }
  }

  interface IYoutubeItemThumbnail {
    url: string;
    width: number;
    height: number;
  }

  interface IActionObject<ActionNameEnum, PayloadInterface> {
    type: ActionNameEnum;
    payload?: PayloadInterface;
  }

  interface IStateFiles {
    searchTerm: string;
    searchInProgress: boolean;
    savedFiles: IYoutubeItem[];
    searchedFiles: IYoutubeItem[];
  }

  interface IModalButton {
    label: string;
    action: () => void;
    class?: string;
  }

  interface IStateModal {
    title: string;
    body: string | JSX.Element;
    buttons?: IModalButton[];
    isActive?: boolean;
  }

  interface IState {
    files: IStateFiles;
    modal: IStateModal;
  }

}

export = Reactiviranje;
