import * as React from 'react';
import './Thumbnail.css';
import {IYoutubeItem} from '../../types';
import ThumbnailAction from './ThumbnailAction';
import { connect } from 'react-redux';

interface IThumbnailProps {
  item: IYoutubeItem;
  selectedItem: IYoutubeItem;
  clickHandler: (item: IYoutubeItem) => void;
}



class Thumbnail extends React.PureComponent<IThumbnailProps> {

  /**
   * Return class for this component.
   */
  public getClassName = () => {
    if (this.props.selectedItem && this.props.item.etag === this.props.selectedItem.etag){
      return 'Thumbnail selected';
    }
    return 'Thumbnail'
  }

  /**
   * Click handler for component. Calls parent component and passes item.
   */
  public handleClick = () => this.props.clickHandler(this.props.item);

  public render() {
    const item = this.props.item;
    return (
      <div className={this.getClassName()}
           onClick={this.handleClick}>
        <div className="thumbnail-image"
             style={{backgroundImage: `url(${item.snippet.thumbnails.medium.url})`}}>
          <ThumbnailAction item={item}/>
        </div>
        <div className="thumbnail-info">
          <p className="thumbnail-title">{item.snippet.title}</p>
          <p className="thumbnail-channel-name">{item.snippet.channelTitle}</p>
          <p className="thumbnail-description">{item.snippet.description}</p>
        </div>
      </div>
    );
  }
}

export default connect()(Thumbnail);
