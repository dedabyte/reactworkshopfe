import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './Search.css';
import Thumbnail from './Thumbnail';
import { IYoutubeItem, IState } from '../../types';
import { aGetSavedFiles, aRequestSearch, aClearSearch } from '../../actions/files'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';


interface IStateSearch {
  // search term
  searchTerm: string;
  //  clicked video from search results
  selectedVideo: IYoutubeItem | null;
}

interface IPropsActions {
  aRequestSearch: typeof aRequestSearch;
  aGetSavedFiles: typeof aGetSavedFiles;
  aClearSearch: typeof aClearSearch;
}

interface IPropsProperties {
  // search term
  searchTerm: string;
  // flag that is active when search is performed and waiting for results
  searchInProgress: boolean;
  // search results
  searchedFiles: IYoutubeItem[];
  // saved files in 'my files' list
  savedFiles: IYoutubeItem[];
}

interface IPropsSearch extends IPropsActions, IPropsProperties, RouteComponentProps<{ searchTerm: string }> { }



class Search extends React.PureComponent<IPropsSearch, IStateSearch> {
  public state: IStateSearch;
  // reference to component's HTMLElement
  private thisNode: Element;

  constructor(props: any) {
    super(props);

    // initialize local state
    this.state = {
      searchTerm: '',
      selectedVideo: null
    };
  }

  public componentDidMount() {
    console.log('Search did mount', this);

    // reference to HTMLElement
    this.thisNode = ReactDOM.findDOMNode(this) as Element;
    this.setState({ searchTerm: this.props.searchTerm });
    this.compareSearchTermsAndTriggerSearch(this.props.searchTerm, this.props.match.params.searchTerm);
  }

  public componentDidUpdate(prevProps: IPropsSearch) {
    const oldQ = prevProps.match.params.searchTerm;
    const newQ = this.props.match.params.searchTerm;

    this.compareSearchTermsAndTriggerSearch(oldQ, newQ);
  }

  /**
   * Handles Enter key and calls search actions.
   * Removes selection.
   * Scrolls results wrap to top.
   * @param event
   */
  public handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.keyCode === 13) {
      // this.setState({ selectedVideo: null });

      const searchTerm = (event.target as HTMLInputElement).value;
      // this.props.aRequestSearch(searchTerm);

      // this.scrollResultsWrapToTop();
      this.props.history.push('/search/' + searchTerm);
    }
  };

  /**
   * Handles click on search results item.
   * @param item
   */
  public handleClick = (item: IYoutubeItem) => {
    this.setState({ selectedVideo: item });
  };

  /**
   * Handles changes on search input and writes local state.
   */
  public handleSearchInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ searchTerm: (event.target as HTMLInputElement).value });
  }

  /**
   * Returns iframe source url for selectred Youtube video.
   */
  public getYTIframeUrl = () => `https://www.youtube.com/embed/${(this.state.selectedVideo as IYoutubeItem).id.videoId}`;

  public render() {
    return (
      <div className="Search">

        <div className="search-panel left">
          <div className="search-box-wrap">
            <input type="text" placeholder="Search for videos..."
              value={this.state.searchTerm}
              onChange={this.handleSearchInputChange}
              onKeyDown={this.handleKeyDown} />
          </div>
          <div className="search-results-wrap">
            {
              this.props.searchInProgress ?
                <div className="spinner" /> :
                this.props.searchedFiles.map(
                  (item: IYoutubeItem) =>
                    <Thumbnail key={item.etag}
                      item={item}
                      selectedItem={this.state.selectedVideo as IYoutubeItem}
                      clickHandler={this.handleClick} />
                )
            }
          </div>
        </div>

        <div className="search-separator" />

        <div className="search-panel right">
          {
            this.state.selectedVideo ?
              <div className="search-video-wrapper">
                <iframe src={this.getYTIframeUrl()} />
              </div> :
              <div className="search-video-empty">
                No video selected.<br />Choose one from search results on the left.
              </div>
          }
        </div>
      </div>
    );
  }

  /**
   * Scrolls list of results to top.
   */
  private scrollResultsWrapToTop() {
    const searchResultsWrapNode = this.thisNode.querySelector('.search-results-wrap');
    if (searchResultsWrapNode) {
      searchResultsWrapNode.scrollTop = 0;
    }
  }

  /**
   * Compares searchTerms and decides whether to do nothing, clear search, or call new search actions.
   * @param oldQ old or previous searchTerm/q
   * @param newQ new or current searchTerm/q
   */
  private compareSearchTermsAndTriggerSearch(oldQ: string, newQ: string) {
    // console.log('compareSearchTermsAndTriggerSearch', oldQ, newQ);

    // do nothing if both search terms are equal or falsy
    if ((oldQ === newQ) || (!oldQ && !newQ)) {
      return;
    }

    // if no new term, call search to be cleared and also clear local state
    if (!newQ) {
      this.setState({ searchTerm: '', selectedVideo: null });
      this.props.aClearSearch();
      return;
    }

    // call a new search and and update local state
    this.setState({ searchTerm: newQ, selectedVideo: null });
    this.props.aRequestSearch(newQ);
    this.scrollResultsWrapToTop();
  }
}

const mapStateToProps = (state: IState): IPropsProperties => {
  return {
    searchTerm: state.files.searchTerm,
    searchInProgress: state.files.searchInProgress,
    searchedFiles: state.files.searchedFiles,
    savedFiles: state.files.savedFiles
  }
}

const actionsToProps: IPropsActions = {
  aRequestSearch,
  aGetSavedFiles,
  aClearSearch
}

export default withRouter(connect(mapStateToProps, actionsToProps)(Search) as any);
