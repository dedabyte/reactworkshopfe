import * as React from 'react';
import './Thumbnail.css';
import { IYoutubeItem } from '../../types';
import { aAddFileToMyList, aRemoveFileFromMyList } from '../../actions/files';
import { connect } from 'react-redux';

interface IPropsActions {
  aAddFileToMyList: typeof aAddFileToMyList;
  aRemoveFileFromMyList: typeof aRemoveFileFromMyList;
}

interface IPropsProperties extends IPropsActions {
  item: IYoutubeItem;
}

interface IPropsThumbnailAction extends IPropsActions, IPropsProperties {}



class ThumbnailAction extends React.PureComponent<IPropsThumbnailAction> {

  /**
   * Return class for this component.
   */
  public getClassName = () => this.props.item.saved ?
    'ThumbnailAction saved' :
    'ThumbnailAction';

  /**
   * Handles click on action.
   */
  public handleClick = (event: React.MouseEvent) => {
    event.stopPropagation();

    this.props.item.saved ?
      this.props.aRemoveFileFromMyList(this.props.item) :
      this.props.aAddFileToMyList(this.props.item);
  }

  public render() {
    const item = this.props.item;
    return (
      <div className={this.getClassName()} onClick={this.handleClick}>{item.saved ? '-' : '+'}</div>
    );
  }
}

const actionsToProps: IPropsActions = {
  aAddFileToMyList,
  aRemoveFileFromMyList
}

export default connect(null, actionsToProps)(ThumbnailAction);
