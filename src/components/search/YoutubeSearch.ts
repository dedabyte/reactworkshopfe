class YoutubeSearch {
  private YOUTUBE_API_KEY = 'AIzaSyBR3X_EGm0vUG1znWWvXpjvXe2Zyr7WW5k';

  public search(searchTerm: string, maxResults: number = 10) {
    const url = this.getSearchUrl(searchTerm, maxResults);
    return fetch(url).then((response) => response.json());
  }

  private getSearchUrl(searchTerm: string, maxResults: number) {
    return `https://www.googleapis.com/youtube/v3/search?q=${searchTerm}=date&part=snippet&type=video&maxResults=${maxResults}&key=${this.YOUTUBE_API_KEY}`;
  }
}

const youtubeSearch = new YoutubeSearch();
export default youtubeSearch;
