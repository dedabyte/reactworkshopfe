import * as React from 'react';
import './SavedFiles.css';
import { IYoutubeItem, IState } from '../../types';
import Thumbnail from '../search/Thumbnail';
import { connect } from 'react-redux';
import { aClearMyList } from '../../actions/files';
import { aOpenModal, aCloseModal } from '../../actions/modal';

interface IStateSavedFIles {
  //  clicked video from search results
  selectedVideo: IYoutubeItem | null;
}

interface IPropsProperties {
  // saved files in 'my files' list
  savedFiles: IYoutubeItem[];
}

interface IPropsActions {
  aClearMyList: typeof aClearMyList;
  aOpenModal: typeof aOpenModal;
  aCloseModal: typeof aCloseModal;
}

interface IPropsSavedFiles extends IPropsProperties, IPropsActions {}



class SavedFiles extends React.PureComponent<IPropsSavedFiles, IStateSavedFIles> {
  public state: IStateSavedFIles;

  constructor(props: any) {
    super(props);

    // initialize local state
    this.state = {
      selectedVideo: null
    };
  }

  /**
   * Handles click on saved files item.
   * @param item
   */
  public selectItem = (item: IYoutubeItem) => {
    this.setState({ selectedVideo: item });
  };

  /**
   * Handles click on clear saved files button. Shows confirmation dialog, attaches 'cancel' and 'confirm' actions.
   */
  public clearList = () => {
    this.props.aOpenModal({
      title: 'Saved files',
      body: <span>U sure u wanna clear the <b>Saved files</b> list?</span>,
      buttons: [
        {
          label: 'Yes',
          action: () => {
            this.props.aClearMyList();
            this.props.aCloseModal();
          }
        },
        {
          label: 'Cancel',
          action: this.props.aCloseModal
        },
      ]
    });
  }

  /**
   * Returns iframe source url for selectred Youtube video.
   */
  public getYTIframeUrl = () => `https://www.youtube.com/embed/${(this.state.selectedVideo as IYoutubeItem).id.videoId}`;

  public render() {
    return (
      <div className="SavedFiles">

        <div className="saved-files-body">
          <div className="search-panel left">
            <div className="saved-files-header">
              Saved files<span title="Clear list" onClick={this.clearList}>X</span>
            </div>
            <div className="search-results-wrap">
              {
                this.props.savedFiles.map(
                  (item: IYoutubeItem) =>
                    <Thumbnail key={item.etag}
                      item={item}
                      selectedItem={this.state.selectedVideo as IYoutubeItem}
                      clickHandler={this.selectItem} />
                )
              }
            </div>
          </div>

          <div className="search-separator" />

          <div className="search-panel right">
            {
              this.state.selectedVideo ?
                <div className="search-video-wrapper">
                  <iframe src={this.getYTIframeUrl()} />
                </div> :
                <div className="search-video-empty">
                  No video selected.<br />Choose one from saved files on the left.
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IState): IPropsProperties => {
  return {
    savedFiles: state.files.savedFiles
  }
}

const actionsToProps: IPropsActions = {
  aClearMyList,
  aOpenModal,
  aCloseModal
}

export default connect(mapStateToProps, actionsToProps)(SavedFiles);
