import './Modal.css';
import * as React from 'react';
import { connect } from 'react-redux';
import { aCloseModal } from '../../actions/modal';
import { IStateModal, IState, IModalButton } from '../../types';

interface IPropsProperties {
  modal: IStateModal
}

interface IPropsActions {
  aCloseModal: typeof aCloseModal
}

interface IPropsModal extends IPropsProperties, IPropsActions { }



class Modal extends React.PureComponent<IPropsModal> {

  public render() {
    const modal = this.props.modal;
    return (
      <div className="Modal">
        <div className="modal-wrapper">
          <div className="modal-header">
            <div className="modal-title">{modal.title}</div>
            <div className="modal-x" onClick={this.props.aCloseModal}>X</div>
          </div>
          <div className="modal-body">{modal.body}</div>
          <div className="modal-footer">
            {
              (modal.buttons || []).map(
                (button: IModalButton, index: number) =>
                  <button className={button.class}
                          key={index}
                          onClick={button.action}>{button.label}</button>
              )
            }
          </div>
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state: IState) => {
  return {
    modal: state.modal
  }
}

const actionsToProps: IPropsActions = {
  aCloseModal
}

export default connect(mapStateToProps, actionsToProps)(Modal);
