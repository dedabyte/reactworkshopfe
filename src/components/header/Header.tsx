import * as React from 'react';
import './Header.css';
import { Link } from 'react-router-dom';
import { IState } from '../../types';
import { connect } from 'react-redux';

interface IPropsProperties {
  // count of saved files in 'my files' list
  savedFilesLength: number;
}



class Header extends React.PureComponent<IPropsProperties> {

  /**
   * If there are saved files, outputs ` (number)` string, to append to 'Saved files' link.
   */
  public generateNumberOfSavedFiles = () =>
    this.props.savedFilesLength ?
      ' (' + this.props.savedFilesLength + ')' :
      '';

  public render() {
    return (
      <div className="Header">
        <Link to={'/search'} className="title">Reactiviranje</Link>
        <Link to={'/savedFiles'} className="link">Saved files{this.generateNumberOfSavedFiles()}</Link>
        {/* <Link to={'/search/reactjs'} className="link">test</Link> */}
      </div>
    );
  }
}

const mapStateToProps = (state: IState): IPropsProperties => {
  return {
    savedFilesLength: state.files.savedFiles.length
  }
}

export default connect(mapStateToProps)(Header);
