import { IYoutubeItem, IActionObject } from '../types';
import { storage, StorageKeys } from '../storage/storage';
import youtubeSearch from '../components/search/YoutubeSearch';
import { Dispatch } from 'redux';

export enum FilesActions {
  GET_SAVED_FILES = 'GET_SAVED_FILES',
  ADD_FILE_TO_MY_LIST = 'ADD_FILE_TO_MY_LIST',
  REMOVE_FILE_FROM_MY_LIST = 'REMOVE_FILE_FROM_MY_LIST',
  CLEAR_MY_LIST = 'CLEAR_MY_LIST',
  REQUEST_SEARCH = 'REQUEST_SEARCH',
  RESPONSE_SEARCH = 'RESPONSE_SEARCH',
  CLEAR_SEARCH = 'CLEAR_SEARCH',
}

export const aRequestSearch = (searchTerm: string) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: FilesActions.REQUEST_SEARCH,
      payload: searchTerm
    });

    youtubeSearch.search(searchTerm).then(
      (response) => {
        dispatch(aResponseSearch(response.items));
      }
    );
  }
}

export const aResponseSearch = (files: IYoutubeItem[]): IActionObject<FilesActions, IYoutubeItem[]> => {
  return {
    type: FilesActions.RESPONSE_SEARCH,
    payload: files
  }
}

export const aClearSearch = (): IActionObject<FilesActions, undefined> => {
  return {
    type: FilesActions.CLEAR_SEARCH
  }
}

export const aGetSavedFiles = (): IActionObject<FilesActions, IYoutubeItem[]> => {
  const savedFiles = storage.get(StorageKeys.myList) || [];
  return {
    type: FilesActions.GET_SAVED_FILES,
    payload: savedFiles
  }
};

export const aAddFileToMyList = (file: IYoutubeItem): IActionObject<FilesActions, IYoutubeItem> => {
  file.saved = true;
  return {
    type: FilesActions.ADD_FILE_TO_MY_LIST,
    payload: file
    // todo
  };
};

export const aRemoveFileFromMyList = (file: IYoutubeItem): IActionObject<FilesActions, IYoutubeItem> => {
  file.saved = false;
  return {
    type: FilesActions.REMOVE_FILE_FROM_MY_LIST,
    payload: file
  };
};

export const aClearMyList = (): IActionObject<FilesActions, undefined> => {
  return {
    type: FilesActions.CLEAR_MY_LIST
  };
}
