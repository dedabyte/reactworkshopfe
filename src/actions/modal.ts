import { IActionObject, IStateModal } from "../types";

export enum ModalActions {
  OPEN_MODAL = 'OPEN_MODAL',
  CLOSE_MODAL = 'CLOSE_MODAL',
}

export const aOpenModal = (config: IStateModal): IActionObject<ModalActions, IStateModal> => {
  return {
    type: ModalActions.OPEN_MODAL,
    payload: config
  }
}

export const aCloseModal = (): IActionObject<ModalActions, undefined> => {
  return {
    type: ModalActions.CLOSE_MODAL
  }
}
